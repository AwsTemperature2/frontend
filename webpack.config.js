const TerserJSPlugin = require('terser-webpack-plugin');
const HtmlWebpackPlugin = require('html-webpack-plugin')
const MiniCssExtractPlugin = require('mini-css-extract-plugin')
const OptimizeCSSAssetsPlugin = require('optimize-css-assets-webpack-plugin');

module.exports = {
    // mode: "development",
    // devtool: "inline-source-map",
    entry: [
      "./src/main.ts",
      "./css/main.css",
    ],
    output: {
      filename: "main.min.js"
    },
    plugins: [
      new HtmlWebpackPlugin({
        template: './src/index.html',
        minify: {
          collapseWhitespace: true,
          removeComments: true
        }
      }),
      new MiniCssExtractPlugin({
          filename: "main.css",

      }),
      new TerserJSPlugin({}), 
      new OptimizeCSSAssetsPlugin({})
    ],
    resolve: {
      extensions: [".ts", ".tsx", ".js"]
    },
    module: {
      rules: [
        { 
          test: /\.tsx?$/, 
          loader: "ts-loader" 
        },
        {
          test: /\.css$/,
          use: [
            {
              loader: MiniCssExtractPlugin.loader,
              options: {
                // you can specify a publicPath here
                // by default it uses publicPath in webpackOptions.output
                publicPath: './dist',
                hmr: process.env.NODE_ENV === 'development',
                importLoaders: 1,
                minimize: true
              },
            },
            'css-loader',
            // 'postcss-loader',
            // "sass-loader" // compiles Sass to CSS
          ],
        }
      ]
    },
    externals: {
      c3: "c3"
    },
    optimization: {
      usedExports: true,
      minimizer: [new TerserJSPlugin({}), new OptimizeCSSAssetsPlugin({})],
    },
    devServer: {
      contentBase: `./dist`,
      compress: false,
      open: false,
      stats: {
        modules: true,
        colors: true,        
      },
    }
  };