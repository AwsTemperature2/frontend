import { Timepoint } from "./timePoint";

export class DataSource {
    public async getData(dateFrom: Date, dateTo: Date, interval: number = 5): Promise<Timepoint[]> {
        if (!dateFrom || isNaN(dateFrom.getTime()) || !dateTo || isNaN(dateTo.getTime())) {
            throw new Error(`None or invalid date selected`);
        }

        console.log(`DataSource.getData: ${dateFrom.toDateString()} - ${dateTo.toDateString()} by ${interval} sec`);

        const urlBase = "https://8tbus9f393.execute-api.eu-central-1.amazonaws.com/dev/temperature";
        const from = dateFrom.toISOString().split("T")[0];
        const to = dateTo.toISOString().split("T")[0];
        const url = `${urlBase}/${interval}/${from}/${to}`;

        if (location.hostname === "localhost" || location.hostname === "127.0.0.1") {
            console.log(`Data source: localhost sample values`);
            return [
               { ts: new Date("2019-01-01 23:30:00").toISOString(), r: 10, h: 31, o: -1},
               { ts: new Date("2019-01-02 00:00:00").toISOString(), r: 11, h: 30, o: -1},
               { ts: new Date("2019-01-02 00:30:00").toISOString(), r: 8, h: 31 },
               { ts: new Date("2019-01-02 01:00:00").toISOString(), r: 9, h: 31 },
               { ts: new Date("2019-01-02 01:30:00").toISOString(), r: 15, h: 31, o: 2}
            ];
        }

        console.log(`Data source url: ${url}`);
        const response = await fetch(url);

        const payload = await response.json();
        return payload.Payload;
    }
}