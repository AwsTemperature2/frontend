import { generate} from "c3";

export class SvgDrawer {
    public draw(data: Array<Array<string | number | boolean>>): void {
        const chart = generate({
            data: {
                x: "Time",
                // xFormat: "%Y-%m-%dT%h:%m:%s", // 'xFormat' can be used as custom format of 'x'
                columns: data
            },
            axis: {
                x: {
                    type: "timeseries",
                    tick: {
                        format: "%Y-%m-%d %H:%M",
                        count: 10,
                        centered: true,
                        fit: true,
                    },
                }
            },
            zoom: {
                enabled: true
            },
            tooltip: {
                show: true
            },
            point: {
                show: false
            },
            grid: {
                x: {
                    show: true
                },
                y: {
                    show: true
                }
            }
        });
    }
}