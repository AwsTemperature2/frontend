import { DataVisualizer } from "./dataVisualizer";

let daysToSub: number = 1;       // # of days to go back from picked date
let daysToAdd: number = 1;       // # of days to go ahead from picked date
let interval: number = 15;

const visualizer = new DataVisualizer();
const loadingIcon = "<img src=\"giphy.gif\"></img>"; // TODO ugly

function addDays(date: Date, days: number) {
    date.setDate(date.getDate() + days);
    return date;
}

function getElement(id: string): HTMLElement {
    return document.getElementById(id);
}

function getSelectedDate(): Date {
    return new Date(document.getElementById("userdate").getAttribute("value"));
}

async function presentData(): Promise<void> {
    const date = getSelectedDate();

    if (!date || isNaN(date.getTime())) {
        console.log(`None or invalid date selected`);
        return;
    }

    console.log(`downloadData based on picked date: ${date}`);

    const dateFrom = addDays(new Date(date), -daysToSub);
    const dateTo = addDays(new Date(date), daysToAdd);

    getElement("chart").innerHTML = loadingIcon;

    const visualiseResult = await visualizer.visualizeData(dateFrom, dateTo, interval);

    if (!visualiseResult) {
        getElement("chart").innerHTML = "<div style=\"margin: 150px\">No data available</div>"; // TODO ugly
    }
}

function checkInterval(button: HTMLElement) {
    getElement("btnDay").removeAttribute("disabled");
    getElement("btnWeek").removeAttribute("disabled");
    getElement("btnMonth").removeAttribute("disabled");
    button.setAttribute("disabled", "true");
}

window.onload = async () => {
    getElement("chart").innerHTML = loadingIcon;
    getElement("btnDay")!.setAttribute("disabled", "true");
    const yesterday = addDays(new Date(), -1);
    getElement("userdate").setAttribute("value", yesterday.toISOString().slice(0, 10));
    await presentData();
};

document.getElementById("userdate")!.addEventListener("change", async (value) => {
    await presentData();
});

document.getElementById("btnDay")!.addEventListener("click", async () => {
    interval = 1;
    daysToAdd = 1;
    daysToSub = 1;
    checkInterval(document.getElementById("btnDay"));
    await presentData();
});

document.getElementById("btnWeek")!.addEventListener("click", async () => {
    interval = 15;
    daysToAdd = 4;
    daysToSub = 4;
    checkInterval(document.getElementById("btnWeek"));
    await presentData();
});

document.getElementById("btnMonth")!.addEventListener("click", async () => {
    interval = 120;
    daysToAdd = 20;
    daysToSub = 5;
    checkInterval(document.getElementById("btnMonth"));
    await presentData();
});
