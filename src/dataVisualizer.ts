import { DataSource } from "./dataSource";
import { SvgDrawer } from "./svgDrawer";
import { Timepoint } from "./timePoint";

export class DataVisualizer {
    private readonly svgDrawer = new SvgDrawer();
    private readonly dataSource = new DataSource();

    public async visualizeData(dateFrom: Date, dateTo: Date, interval: number): Promise<boolean> {
        const data: Timepoint[] = await this.dataSource.getData(dateFrom, dateTo, interval);

        if (!data || data.length === 0) {
            console.log("No data fetched from data source");
            return false;
        }

        const transformedData = this.trasformData(data);

        this.svgDrawer.draw(transformedData);
        return true;
    }

    private trasformData(data: Timepoint[]): Array<Array<string | number | boolean>> {
        const tvals = data.map((p) => new Date(p.ts) as Date | string);
        tvals.unshift("Time");

        const rvals = data.map((p) => p.r ? p.r : null as any);
        rvals.unshift("Room");

        const hvals = data.map((p) => p.h ? p.h : null as any);
        hvals.unshift("Heat");

        const ovals = data.map((p) => p.o ? p.o : null as any);
        ovals.unshift("Out");

        return [
            tvals,
            rvals,
            hvals,
            ovals
        ];
    }
}