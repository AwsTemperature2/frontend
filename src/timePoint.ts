export class Timepoint {
    public ts: string;
    public r?: number;
    public h?: number;
    public o?: number;
}