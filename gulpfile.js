var gulp = require('gulp');
var cleanCSS = require('gulp-clean-css'); 
var runSequence = require('run-sequence');
const minify = require('gulp-minify');
var htmlmin = require('gulp-htmlmin');
var clean = require('gulp-clean');
 
gulp.task('clean', function () {
    return gulp.src('./dist')
        .pipe(clean({force: true}))
}); 
 
gulp.task('clean-nonminified', function () {
    return gulp.src('./dist/main.js')
        .pipe(clean({force: true}))
}); 
 

gulp.task('default', function(done) {
    runSequence('clean', 'minify-js', 'minify-css', 'minify-html', 'clean-nonminified', function() {
        console.log('All done');
        done();
    });
}); 
 
 
gulp.task('minify-js', function() {
  gulp.src('src/main.js')
    .pipe(minify({
        ext:{
            min:'.min.js'
        },
    }))
    .pipe(gulp.dest('./dist'))
});


gulp.task('minify-css', () => {
  return gulp.src('src/*.css')
    .pipe(cleanCSS({compatibility: 'ie8'}))
    .pipe(gulp.dest('./dist'));
});

gulp.task('minify-html', function() {
  return gulp.src('src/*.html')
    .pipe(htmlmin({collapseWhitespace: true}))
    .pipe(gulp.dest('./dist'));
});

gulp.task('copy-html', function () {
    gulp.src('./src/index.html')
        .pipe(gulp.dest('./dist/'));
});